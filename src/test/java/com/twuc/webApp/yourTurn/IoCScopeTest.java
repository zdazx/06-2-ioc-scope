package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;

public class IoCScopeTest {
    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp(){
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
    }

    // 2.1 -1
    @Test
    void should_verify_that_is_the_same_bean() {
        InterfaceOne interface1 = context.getBean(InterfaceOne.class);
        InterfaceOneImpl interfaceImpl = context.getBean(InterfaceOneImpl.class);
        assertSame(interface1,interfaceImpl);
    }

    // 2.1 -2
    @Test
    void should_verify_extends_and_base_class_is_same() {
        BaseClassOne baseClassOne = context.getBean(BaseClassOne.class);
        ExtendsClassOne extendsClassOne = context.getBean(ExtendsClassOne.class);
        assertSame(baseClassOne, extendsClassOne);
    }

    // 2.1 -3
    @Test
    void should_verify_bean_abstract_class_and_derived_class() {
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        assertSame(abstractBaseClass, derivedClass);
    }

    // 2.2 -1
    @Test
    void should_verify_instance_is_same_in_prototype_of_scope() {
        SimplePrototypeScopeClass scopeClass1 = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass scopeClass2 = context.getBean(SimplePrototypeScopeClass.class);
        assertNotSame(scopeClass1, scopeClass2);
    }

    // 2.2 -2

    // 2.2 -3

    // 2.2 -4
    @Test
    void should_verify_instance_when_prototype_depend_on_singleton() {
        PrototypeScopeDependsOnSingleton dependsOnSingleton1 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton dependsOnSingleton2 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        assertNotSame(dependsOnSingleton1, dependsOnSingleton2);
        assertSame(dependsOnSingleton1.getSingletonDependent(), dependsOnSingleton2.getSingletonDependent());
    }

    // 2.2 -5
    @Test
    void should_verify_instance_when_singleton_depend_on_prototype() {
        SingletonDependsOnPrototype dependsOnPrototype1 = context.getBean(SingletonDependsOnPrototype.class);
        SingletonDependsOnPrototype dependsOnPrototype2 = context.getBean(SingletonDependsOnPrototype.class);
        assertSame(dependsOnPrototype1, dependsOnPrototype2);
        assertSame(dependsOnPrototype1.getPrototypeDependent(), dependsOnPrototype2.getPrototypeDependent());
    }
}
